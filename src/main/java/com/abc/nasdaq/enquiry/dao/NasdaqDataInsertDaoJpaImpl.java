package com.abc.nasdaq.enquiry.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataInsertDaoRequestEntity;
import com.abc.nasdaq.persistence.model.NasdaqData;

@Repository
public class NasdaqDataInsertDaoJpaImpl
{
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void send(NasdaqDataInsertDaoRequestEntity request)
    {
	NasdaqData nasdaqData = new NasdaqData();
	nasdaqData.setNasdaqDate(request.getTargetDate());
	nasdaqData.setNasdaqRawData(request.getRawData());

	em.persist(nasdaqData);
    }
}
