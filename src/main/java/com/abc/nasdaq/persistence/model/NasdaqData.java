package com.abc.nasdaq.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "nasdaq_data")
public class NasdaqData
{
    @Id
    @Temporal(TemporalType.DATE)
    @Column(name = "nasdaq_date")
    private Date nasdaqDate;

    @Column(name = "nasdaq_raw_data")
    private String nasdaqRawData;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "updated", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    public Date getNasdaqDate()
    {
	return nasdaqDate;
    }

    public void setNasdaqDate(Date nasdaqDate)
    {
	this.nasdaqDate = nasdaqDate;
    }

    public String getNasdaqRawData()
    {
	return nasdaqRawData;
    }

    public void setNasdaqRawData(String nasdaqRawData)
    {
	this.nasdaqRawData = nasdaqRawData;
    }

    public Date getCreated()
    {
	return created;
    }

    public void setCreated(Date created)
    {
	this.created = created;
    }

    public Date getUpdated()
    {
	return updated;
    }

    public void setUpdated(Date updated)
    {
	this.updated = updated;
    }

    @Override
    public String toString()
    {
	return "NasdaqData [nasdaqDate=" + nasdaqDate + ", nasdaqRawData=" + nasdaqRawData + ", created=" + created + ", updated=" + updated + "]";
    }

}
