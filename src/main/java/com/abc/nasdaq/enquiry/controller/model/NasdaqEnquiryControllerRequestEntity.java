package com.abc.nasdaq.enquiry.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NasdaqEnquiryControllerRequestEntity
{
    @JsonProperty("auth_token")
    private String authToken;

    @JsonProperty("target_date")
    private String targetDate;

    public String getAuthToken()
    {
	return authToken;
    }

    public void setAuthToken(String authToken)
    {
	this.authToken = authToken;
    }

    public String getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(String targetDate)
    {
	this.targetDate = targetDate;
    }
}
