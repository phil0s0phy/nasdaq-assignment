package com.abc.nasdaq.datafetcher.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataUpdateDaoRequestEntity;

@Repository
public class NasdaqDataUpdateDaoJpaImpl
{
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void send(NasdaqDataUpdateDaoRequestEntity request)
    {
	String queryString = "UPDATE NasdaqData SET nasdaqRawData = :nasdaqRawData, updated = CURRENT_TIMESTAMP WHERE nasdaqDate = :nasdaqDate";
	Query query = em.createQuery(queryString);

	query.setParameter("nasdaqRawData", request.getRawData());
	query.setParameter("nasdaqDate", request.getTargetDate());

	query.executeUpdate();
    }
}
