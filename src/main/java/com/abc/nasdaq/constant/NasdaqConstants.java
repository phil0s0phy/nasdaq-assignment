package com.abc.nasdaq.constant;

public class NasdaqConstants
{
    public static final String INPUT_DATE_FORMAT = "yyyy-MM-dd";

    public static final String HEADER_X_AUTH_TOKEN = "x-auth-token";
    public static final String HEADER_X_TARGET_DATE = "x-target-date";

    public static final String RESPONSE_MESSAGE_OK = "ok";
    public static final String RESPONSE_MESSAGE_INCORRECT_DATE_FORMAT = "Date format must be yyyy-MM-dd";
    public static final String RESPONSE_MESSAGE_INVALID_AUTH_TOKEN = "Invalid authentication token";
}
