package com.abc.nasdaq.datafetcher.service;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.abc.nasdaq.datafetcher.dao.NasdaqDataUpdateDaoJpaImpl;
import com.abc.nasdaq.datafetcher.dao.NasdaqOnlineDataFetcherDaoRestImpl;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataInsertDaoRequestEntity;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataUpdateDaoRequestEntity;
import com.abc.nasdaq.enquiry.dao.NasdaqDataInsertDaoJpaImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ NasdaqOnlineDataFetcherDaoRestImpl.class, NasdaqOnlineDataFetcherService.class })
public class NasdaqOnlineDataFetcherServiceTest
{
    private NasdaqOnlineDataFetcherService nasdaqOnlineDataFetcherService;
    private NasdaqOnlineDataFetcherDaoRestImpl nasdaqOnlineDataFetcherDaoRestImpl;

    @Mock
    private NasdaqDataInsertDaoJpaImpl nasdaqDataInsertDaoJpaImpl;

    @Mock
    private NasdaqDataUpdateDaoJpaImpl nasdaqDataUpdateDaoJpaImpl;

    @Mock
    private EntityManager em;

    @Mock
    private Query query;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ResponseEntity<String> nasdaqResponse;

    private NasdaqDataInsertDaoRequestEntity nasdaqDataInsertDaoRequest;
    private NasdaqDataUpdateDaoRequestEntity nasdaqDataUpdateDaoRequest;

    @Before
    public void setUp() throws Exception
    {
	nasdaqOnlineDataFetcherService = new NasdaqOnlineDataFetcherService();
	nasdaqOnlineDataFetcherDaoRestImpl = new NasdaqOnlineDataFetcherDaoRestImpl();

	nasdaqDataInsertDaoRequest = new NasdaqDataInsertDaoRequestEntity();
	nasdaqDataUpdateDaoRequest = new NasdaqDataUpdateDaoRequestEntity();

	Whitebox.setInternalState(nasdaqOnlineDataFetcherService, "nasdaqOnlineDataFetcherDaoRestImpl", nasdaqOnlineDataFetcherDaoRestImpl);
	Whitebox.setInternalState(nasdaqOnlineDataFetcherService, "nasdaqDataInsertDaoJpaImpl", nasdaqDataInsertDaoJpaImpl);
	Whitebox.setInternalState(nasdaqOnlineDataFetcherService, "nasdaqDataUpdateDaoJpaImpl", nasdaqDataUpdateDaoJpaImpl);

	PowerMockito.when(em.createQuery(Matchers.anyString())).thenReturn(query);
	PowerMockito.whenNew(RestTemplate.class).withAnyArguments().thenReturn(restTemplate);

	PowerMockito.whenNew(NasdaqDataInsertDaoRequestEntity.class).withNoArguments().thenReturn(nasdaqDataInsertDaoRequest);
	PowerMockito.whenNew(NasdaqDataUpdateDaoRequestEntity.class).withNoArguments().thenReturn(nasdaqDataUpdateDaoRequest);
    }

    @Test
    public void testSuccess_firstTimeFetch()
    {
	long targetDateLong = 1445015764000L;
	Date targetDateToInsert = new Date(targetDateLong - TimeZone.getDefault().getOffset(0L));
	// Given
	String nasdaqReturnedString = "{\"data\":[{\"x\":1444987859000,\"y\":4874.11},{\"x\":1444987919000,\"y\":4874.1}],\"AsOf\":"
		+ targetDateLong
		+ ",\"Change\":\"16.590000\",\"chart_begin_time\":1444987800000,\"chart_end_time\":1445011200000,\"Index\":\"IXIC\",\"Open\":4870.1,\"High\":\"4886.960000\",\"Low\":\"4851.280000\",\"DataMin\":4852.1,\"DataMax\":4886.69,\"PreviousClose\":4870.1,\"Value\":\"4886.690000\",\"Time\":1445188560000,\"Volume\":\"1749571830\"}";

	// When
	PowerMockito.when(restTemplate.postForEntity(Matchers.anyString(), Matchers.any(MultiValueMap.class), Matchers.eq(String.class))).thenReturn(
		nasdaqResponse);
	PowerMockito.when(nasdaqResponse.getBody()).thenReturn(nasdaqReturnedString);

	nasdaqOnlineDataFetcherService.doService();

	// Then
	assertEquals(targetDateToInsert, nasdaqDataInsertDaoRequest.getTargetDate());
	assertNotNull(nasdaqReturnedString, nasdaqDataInsertDaoRequest.getRawData());

	PowerMockito.verifyZeroInteractions(nasdaqDataUpdateDaoJpaImpl);
    }

    @Test
    public void testSuccess_secontTimeFetch()
    {
	long targetDateLong = 1445015764000L;
	Date targetDateToInsert = new Date(targetDateLong - TimeZone.getDefault().getOffset(0L));
	// Given
	String nasdaqReturnedString = "{\"data\":[{\"x\":1444987859000,\"y\":4874.11},{\"x\":1444987919000,\"y\":4874.1}],\"AsOf\":"
		+ targetDateLong
		+ ",\"Change\":\"16.590000\",\"chart_begin_time\":1444987800000,\"chart_end_time\":1445011200000,\"Index\":\"IXIC\",\"Open\":4870.1,\"High\":\"4886.960000\",\"Low\":\"4851.280000\",\"DataMin\":4852.1,\"DataMax\":4886.69,\"PreviousClose\":4870.1,\"Value\":\"4886.690000\",\"Time\":1445188560000,\"Volume\":\"1749571830\"}";

	// When
	PowerMockito.when(restTemplate.postForEntity(Matchers.anyString(), Matchers.any(MultiValueMap.class), Matchers.eq(String.class))).thenReturn(
		nasdaqResponse);
	PowerMockito.when(nasdaqResponse.getBody()).thenReturn(nasdaqReturnedString);

	PowerMockito.doThrow(new RuntimeException("duplicate")).when(nasdaqDataInsertDaoJpaImpl)
		.send(Matchers.any(NasdaqDataInsertDaoRequestEntity.class));

	nasdaqOnlineDataFetcherService.doService();

	// Then
	assertEquals(targetDateToInsert, nasdaqDataInsertDaoRequest.getTargetDate());
	assertNotNull(nasdaqReturnedString, nasdaqDataInsertDaoRequest.getRawData());

	assertEquals(targetDateToInsert, nasdaqDataUpdateDaoRequest.getTargetDate());
	assertNotNull(nasdaqReturnedString, nasdaqDataUpdateDaoRequest.getRawData());
    }

}
