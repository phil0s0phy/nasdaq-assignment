package com.abc.nasdaq.common.configuration;

import javax.servlet.ServletContext;

import com.mangofactory.swagger.paths.SwaggerPathProvider;

public class RelativeSwaggerPathProvider extends SwaggerPathProvider
{
    private final ServletContext servletContext;

    public RelativeSwaggerPathProvider(ServletContext servletContext)
    {
	super();
	this.servletContext = servletContext;
    }

    @Override
    protected String applicationPath()
    {
	return new StringBuilder(servletContext.getContextPath()).append("/api").toString();
    }

    @Override
    protected String getDocumentationPath()
    {
	return "/";
    }

}
