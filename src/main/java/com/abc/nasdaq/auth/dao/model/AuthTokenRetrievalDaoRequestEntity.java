package com.abc.nasdaq.auth.dao.model;

public class AuthTokenRetrievalDaoRequestEntity
{
    private String authToken;

    public String getAuthToken()
    {
	return authToken;
    }

    public void setAuthToken(String authToken)
    {
	this.authToken = authToken;
    }

    @Override
    public String toString()
    {
	return "AuthenticationServiceRequestEntity [authToken=" + authToken + "]";
    }
}
