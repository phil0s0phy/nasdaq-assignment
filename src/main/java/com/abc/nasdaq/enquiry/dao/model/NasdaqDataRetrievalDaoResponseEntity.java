package com.abc.nasdaq.enquiry.dao.model;

import java.util.List;

import com.abc.nasdaq.model.NasdaqIndexValue;

public class NasdaqDataRetrievalDaoResponseEntity
{
    private List<NasdaqIndexValue> nasdaqIndexValueList;

    public List<NasdaqIndexValue> getNasdaqIndexValueList()
    {
	return nasdaqIndexValueList;
    }

    public void setNasdaqIndexValueList(List<NasdaqIndexValue> nasdaqIndexValueList)
    {
	this.nasdaqIndexValueList = nasdaqIndexValueList;
    }
}
