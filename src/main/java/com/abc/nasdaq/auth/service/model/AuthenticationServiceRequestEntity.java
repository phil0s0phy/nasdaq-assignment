package com.abc.nasdaq.auth.service.model;

public class AuthenticationServiceRequestEntity
{
    private String authToken;

    public String getAuthToken()
    {
	return authToken;
    }

    public void setAuthToken(String authToken)
    {
	this.authToken = authToken;
    }

    @Override
    public String toString()
    {
	return "AuthenticationServiceRequestEntity [authToken=" + authToken + "]";
    }

}
