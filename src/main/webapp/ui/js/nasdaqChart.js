function fetchNasdaqData() {
	$.ajax({
		url : "/api/enquiry",
		type : "GET",
		beforeSend : function(xhr) {
			xhr.setRequestHeader('x-auth-token', 'validToken');
			xhr.setRequestHeader('x-target-date',
					$("#datepicker").val() == undefined ? '2015-10-16' : $(
							"#datepicker").val());
		},
		success : function(data) {
			updateChart(data)
		}
	});
};

function updateChart(chartData) {

	$('#container').highcharts({
		title : {
			text : 'NASDAQ',
			x : -20
		// center
		},
		xAxis : {
			type : 'datetime'
		},
		yAxis : {
			title : {
				text : chartData.target_date
			}
		},
		legend : {
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'middle',
			borderWidth : 0
		},
		series : [ {
			name : 'Index',
			data : chartData.nasdaq_index_value_list
		} ]
	});
};

$(function() {
	$("#datepicker").datepicker({
		"dateFormat" : "yy-mm-dd"
	});
});

fetchNasdaqData();

window.setInterval(function() {
	fetchNasdaqData();
}, 60000);