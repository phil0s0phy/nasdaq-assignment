package com.abc.nasdaq.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "auth_token")
public class AuthToken
{
    @Id
    @Column(name = "token")
    private String token;

    public String getToken()
    {
	return token;
    }

    public void setToken(String token)
    {
	this.token = token;
    }

}
