package com.abc.nasdaq.enquiry.service.model;

import java.util.Date;

public class NasdaqEnquiryServiceRequestEntity
{
    private Date targetDate;

    public Date getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(Date targetDate)
    {
	this.targetDate = targetDate;
    }

    @Override
    public String toString()
    {
	return "NasdaqEnquiryServiceRequestEntity [targetDate=" + targetDate + "]";
    }

}
