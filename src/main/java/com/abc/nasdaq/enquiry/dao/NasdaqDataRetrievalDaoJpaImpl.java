package com.abc.nasdaq.enquiry.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.abc.nasdaq.common.dao.exception.DaoException;
import com.abc.nasdaq.enquiry.dao.model.NasdaqDataRetrievalDaoRequestEntity;
import com.abc.nasdaq.enquiry.dao.model.NasdaqDataRetrievalDaoResponseEntity;
import com.abc.nasdaq.model.NasdaqDataResponse;
import com.abc.nasdaq.model.NasdaqIndexValue;
import com.abc.nasdaq.model.NasdaqRawData;
import com.abc.nasdaq.persistence.model.NasdaqData;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class NasdaqDataRetrievalDaoJpaImpl
{
    private Logger logger = Logger.getLogger(this.getClass());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public NasdaqDataRetrievalDaoResponseEntity send(NasdaqDataRetrievalDaoRequestEntity request)
    {
	try
	{
	    String queryStr = "FROM NasdaqData nd WHERE nd.nasdaqDate = :targetDate";
	    Query query = em.createQuery(queryStr);
	    query.setParameter("targetDate", request.getTargetDate());

	    List resultList = query.getResultList();
	    List<NasdaqIndexValue> nasdaqIndexValueList = new ArrayList<NasdaqIndexValue>();

	    if (resultList.size() > 0)
	    {
		NasdaqData nasdaqData = (NasdaqData) resultList.get(0);
		logger.debug(nasdaqData);
		ObjectMapper mapper = new ObjectMapper();
		NasdaqDataResponse nasdaqDataResponse = mapper.readValue(nasdaqData.getNasdaqRawData(), NasdaqDataResponse.class);

		for (NasdaqRawData nasdaqRawData : nasdaqDataResponse.getData())
		{
		    NasdaqIndexValue nasdaqIndexValue = new NasdaqIndexValue();
		    nasdaqIndexValue.setTimestamp(nasdaqRawData.getTimestamp());
		    nasdaqIndexValue.setValue(nasdaqRawData.getValue());

		    nasdaqIndexValueList.add(nasdaqIndexValue);
		}
	    }

	    NasdaqDataRetrievalDaoResponseEntity response = new NasdaqDataRetrievalDaoResponseEntity();
	    response.setNasdaqIndexValueList(nasdaqIndexValueList);

	    return response;
	} catch (Exception e)
	{
	    logger.error(e);
	    throw new DaoException();
	}
    }
}
