package com.abc.nasdaq.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NasdaqDataResponse
{
    @JsonProperty("data")
    private List<NasdaqRawData> data;

    @JsonProperty("AsOf")
    private long targetDate;

    public List<NasdaqRawData> getData()
    {
	return data;
    }

    public void setData(List<NasdaqRawData> data)
    {
	this.data = data;
    }

    public long getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(long targetDate)
    {
	this.targetDate = targetDate;
    }

    @Override
    public String toString()
    {
	return "NasdaqDataResponse [data=" + data + ", targetDate=" + targetDate + "]";
    }

}
