package com.abc.nasdaq.enquiry.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import com.abc.nasdaq.auth.dao.AuthTokenRetrievalDaoJpaImpl;
import com.abc.nasdaq.auth.service.AuthenticationService;
import com.abc.nasdaq.constant.NasdaqConstants;
import com.abc.nasdaq.enquiry.controller.model.NasdaqEnquiryControllerRequestEntity;
import com.abc.nasdaq.enquiry.controller.model.NasdaqEnquiryControllerResponseEntity;
import com.abc.nasdaq.enquiry.dao.NasdaqDataRetrievalDaoJpaImpl;
import com.abc.nasdaq.enquiry.service.NasdaqEnquiryService;
import com.abc.nasdaq.model.NasdaqIndexValue;
import com.abc.nasdaq.persistence.model.AuthToken;
import com.abc.nasdaq.persistence.model.NasdaqData;

@RunWith(PowerMockRunner.class)
public class NasdaqEnquiryControllerTest
{
    private NasdaqEnquiryController nasdaqEnquiryController;
    private AuthenticationService authenticationService;
    private NasdaqEnquiryService nasdaqEnquiryService;
    private AuthTokenRetrievalDaoJpaImpl authTokenRetrievalDaoJpaImpl;
    private NasdaqDataRetrievalDaoJpaImpl nasdaqDataRetrievalDaoJpaImpl;

    @Mock
    private HttpServletRequest httpServletRequest;
    private HttpServletResponse httpServletResponse;

    @Mock
    private EntityManager em;

    @Mock
    private Query query;

    @Before
    public void setUp() throws Exception
    {
	nasdaqEnquiryController = new NasdaqEnquiryController();
	authenticationService = new AuthenticationService();
	nasdaqEnquiryService = new NasdaqEnquiryService();
	authTokenRetrievalDaoJpaImpl = new AuthTokenRetrievalDaoJpaImpl();
	nasdaqDataRetrievalDaoJpaImpl = new NasdaqDataRetrievalDaoJpaImpl();

	httpServletResponse = new MockHttpServletResponse();

	Whitebox.setInternalState(nasdaqEnquiryController, "nasdaqEnquiryService", nasdaqEnquiryService);
	Whitebox.setInternalState(nasdaqEnquiryController, "authenticationService", authenticationService);
	Whitebox.setInternalState(nasdaqEnquiryService, "nasdaqDataRetrievalDaoJpaImpl", nasdaqDataRetrievalDaoJpaImpl);
	Whitebox.setInternalState(authenticationService, "authTokenRetrievalDaoJpaImpl", authTokenRetrievalDaoJpaImpl);
	Whitebox.setInternalState(nasdaqDataRetrievalDaoJpaImpl, "em", em);
	Whitebox.setInternalState(authTokenRetrievalDaoJpaImpl, "em", em);

	PowerMockito.when(em.createQuery(Matchers.anyString())).thenReturn(query);
    }

    @Test
    public void testSuccess_retrieveDataFromDb()
    {
	// Given
	String authToken = "authToken";
	String targetDate = "2015-10-17";

	long firstTimestamp = 1111111111L;
	BigDecimal firstValue = new BigDecimal(1111.1123);

	long secondTimestamp = 2222222222L;
	BigDecimal secondValue = new BigDecimal(2222.2234);

	// When
	String nasdaqRawData = "{\"data\":[{\"x\":\"" + firstTimestamp + "\",\"y\":\"" + firstValue + "\"},{\"x\":\"" + secondTimestamp
		+ "\",\"y\":\"" + secondValue + "\"}],\"AsOf\":1445015764000}";

	List<AuthToken> authTokenResultList = new ArrayList<AuthToken>();
	authTokenResultList.add(new AuthToken());

	Calendar calendar = Calendar.getInstance();
	calendar.set(2015, Calendar.OCTOBER, 17);

	List<NasdaqData> nasdaqDataResultList = new ArrayList<NasdaqData>();
	NasdaqData nasdaqData = new NasdaqData();
	nasdaqData.setNasdaqDate(calendar.getTime());
	nasdaqData.setNasdaqRawData(nasdaqRawData);
	nasdaqDataResultList.add(nasdaqData);

	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_AUTH_TOKEN)).thenReturn(authToken);
	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_TARGET_DATE)).thenReturn(targetDate);

	PowerMockito.when(query.getResultList()).thenReturn(authTokenResultList, nasdaqDataResultList);

	NasdaqEnquiryControllerRequestEntity request = new NasdaqEnquiryControllerRequestEntity();
	request.setAuthToken(authToken);
	request.setTargetDate(targetDate);
	NasdaqEnquiryControllerResponseEntity response = nasdaqEnquiryController.enquiry(httpServletRequest, httpServletResponse);

	// Then
	assertEquals(HttpStatus.OK.value(), httpServletResponse.getStatus());

	assertEquals(NasdaqConstants.RESPONSE_MESSAGE_OK, response.getMessage());

	assertEquals(targetDate, response.getTargetDate());
	assertEquals(2, response.getNasdaqIndexValueList().size());

	NasdaqIndexValue firstNasdaqIndexValue = response.getNasdaqIndexValueList().get(0);
	assertEquals(firstTimestamp, firstNasdaqIndexValue.getTimestamp());
	assertEquals(firstValue, firstNasdaqIndexValue.getValue());

	NasdaqIndexValue secondNasdaqIndexValue = response.getNasdaqIndexValueList().get(1);
	assertEquals(secondTimestamp, secondNasdaqIndexValue.getTimestamp());
	assertEquals(secondValue, secondNasdaqIndexValue.getValue());
    }

    @Test
    public void testFail_incorrectDateFormat()
    {
	// Given
	String authToken = "authToken";
	String targetDate = "2015-Oct-17";

	List<AuthToken> authTokenResultList = new ArrayList<AuthToken>();
	authTokenResultList.add(new AuthToken());

	// When
	PowerMockito.when(query.getResultList()).thenReturn(authTokenResultList);

	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_AUTH_TOKEN)).thenReturn(authToken);
	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_TARGET_DATE)).thenReturn(targetDate);

	NasdaqEnquiryControllerRequestEntity request = new NasdaqEnquiryControllerRequestEntity();
	request.setAuthToken(authToken);
	request.setTargetDate(targetDate);
	NasdaqEnquiryControllerResponseEntity response = nasdaqEnquiryController.enquiry(httpServletRequest, httpServletResponse);

	// Then
	assertEquals(HttpStatus.BAD_REQUEST.value(), httpServletResponse.getStatus());
	assertEquals(targetDate, response.getTargetDate());
	assertEquals(NasdaqConstants.RESPONSE_MESSAGE_INCORRECT_DATE_FORMAT, response.getMessage());
	assertNull(response.getNasdaqIndexValueList());
    }

    @Test
    public void testFail_invalidAuthToken()
    {
	// Given
	String authToken = "invalidAuthToken";
	String targetDate = "2015-10-17";

	List<AuthToken> authTokenResultList = new ArrayList<AuthToken>();

	// When
	PowerMockito.when(query.getResultList()).thenReturn(authTokenResultList);

	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_AUTH_TOKEN)).thenReturn(authToken);
	PowerMockito.when(httpServletRequest.getHeader(NasdaqConstants.HEADER_X_TARGET_DATE)).thenReturn(targetDate);

	NasdaqEnquiryControllerRequestEntity request = new NasdaqEnquiryControllerRequestEntity();
	request.setAuthToken(authToken);
	request.setTargetDate(targetDate);
	NasdaqEnquiryControllerResponseEntity response = nasdaqEnquiryController.enquiry(httpServletRequest, httpServletResponse);

	// Then
	assertEquals(HttpStatus.UNAUTHORIZED.value(), httpServletResponse.getStatus());
	assertEquals(targetDate, response.getTargetDate());
	assertEquals(NasdaqConstants.RESPONSE_MESSAGE_INVALID_AUTH_TOKEN, response.getMessage());
	assertNull(response.getNasdaqIndexValueList());
    }

}
