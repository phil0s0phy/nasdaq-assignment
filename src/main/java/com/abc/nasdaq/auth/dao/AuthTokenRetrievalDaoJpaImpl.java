package com.abc.nasdaq.auth.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.abc.nasdaq.auth.dao.model.AuthTokenRetrievalDaoRequestEntity;
import com.abc.nasdaq.common.dao.exception.TransactionNotFoundException;

@Repository
public class AuthTokenRetrievalDaoJpaImpl
{
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void send(AuthTokenRetrievalDaoRequestEntity request) throws TransactionNotFoundException
    {
	String queryStr = "FROM AuthToken at WHERE at.token = :token";
	Query query = em.createQuery(queryStr);
	query.setParameter("token", request.getAuthToken());

	if (query.getResultList().size() <= 0)
	{
	    throw new TransactionNotFoundException();
	}
    }

}
