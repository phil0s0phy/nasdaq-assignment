package com.abc.nasdaq.enquiry.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.nasdaq.enquiry.dao.NasdaqDataRetrievalDaoJpaImpl;
import com.abc.nasdaq.enquiry.dao.model.NasdaqDataRetrievalDaoRequestEntity;
import com.abc.nasdaq.enquiry.dao.model.NasdaqDataRetrievalDaoResponseEntity;
import com.abc.nasdaq.enquiry.service.model.NasdaqEnquiryServiceRequestEntity;
import com.abc.nasdaq.enquiry.service.model.NasdaqEnquiryServiceResponseEntity;

@Service
public class NasdaqEnquiryService
{
    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private NasdaqDataRetrievalDaoJpaImpl nasdaqDataRetrievalDaoJpaImpl;

    public NasdaqEnquiryServiceResponseEntity doService(NasdaqEnquiryServiceRequestEntity request)
    {
	NasdaqDataRetrievalDaoRequestEntity nasdaqDataRetrievalDaoRequest = new NasdaqDataRetrievalDaoRequestEntity();
	nasdaqDataRetrievalDaoRequest.setTargetDate(request.getTargetDate());
	NasdaqDataRetrievalDaoResponseEntity nasdaqDataRetrievalDaoResponse = nasdaqDataRetrievalDaoJpaImpl.send(nasdaqDataRetrievalDaoRequest);

	NasdaqEnquiryServiceResponseEntity response = new NasdaqEnquiryServiceResponseEntity();
	response.setNasdaqIndexValueList(nasdaqDataRetrievalDaoResponse.getNasdaqIndexValueList());

	return response;
    }

}
