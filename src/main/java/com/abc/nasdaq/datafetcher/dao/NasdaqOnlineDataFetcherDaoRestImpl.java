package com.abc.nasdaq.datafetcher.dao;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.abc.nasdaq.common.dao.exception.DaoException;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqOnlineDataFetcherDaoRequestEntity;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqOnlineDataFetcherDaoResponseEntity;
import com.abc.nasdaq.model.NasdaqDataResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class NasdaqOnlineDataFetcherDaoRestImpl
{
    private Logger logger = Logger.getLogger(this.getClass());

    @Transactional
    public NasdaqOnlineDataFetcherDaoResponseEntity send(NasdaqOnlineDataFetcherDaoRequestEntity request)
    {
	try
	{
	    SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
	    simpleClientHttpRequestFactory.setConnectTimeout(request.getConnectTimeout());
	    simpleClientHttpRequestFactory.setReadTimeout(request.getReadTimeout());

	    RestTemplate restTemplate = new RestTemplate(simpleClientHttpRequestFactory);

	    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	    map.add("index", request.getIndex());

	    ResponseEntity<String> nasdaqResponse = restTemplate.postForEntity(request.getUrl(), map, String.class);
	    String jsonResponse = nasdaqResponse.getBody();

	    ObjectMapper mapper = new ObjectMapper();
	    NasdaqDataResponse nasdaqDataResponse = mapper.readValue(jsonResponse, NasdaqDataResponse.class);

	    NasdaqOnlineDataFetcherDaoResponseEntity response = new NasdaqOnlineDataFetcherDaoResponseEntity();
	    response.setNasdaqDataResponse(nasdaqDataResponse);

	    return response;
	} catch (Exception e)
	{
	    logger.error(e);
	    throw new DaoException();
	}
    }
}
