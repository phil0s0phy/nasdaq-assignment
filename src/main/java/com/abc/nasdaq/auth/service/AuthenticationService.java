package com.abc.nasdaq.auth.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.nasdaq.auth.dao.AuthTokenRetrievalDaoJpaImpl;
import com.abc.nasdaq.auth.dao.model.AuthTokenRetrievalDaoRequestEntity;
import com.abc.nasdaq.auth.service.model.AuthenticationServiceRequestEntity;
import com.abc.nasdaq.common.dao.exception.TransactionNotFoundException;
import com.abc.nasdaq.common.exception.AuthenticationFailException;

@Service
public class AuthenticationService
{
    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private AuthTokenRetrievalDaoJpaImpl authTokenRetrievalDaoJpaImpl;

    public void doService(AuthenticationServiceRequestEntity request) throws AuthenticationFailException
    {
	AuthTokenRetrievalDaoRequestEntity authTokenRetrievalDaoRequest = new AuthTokenRetrievalDaoRequestEntity();
	authTokenRetrievalDaoRequest.setAuthToken(request.getAuthToken());

	try
	{
	    authTokenRetrievalDaoJpaImpl.send(authTokenRetrievalDaoRequest);
	} catch (TransactionNotFoundException e)
	{
	    logger.debug("Invalid authentication with token: " + request.getAuthToken());
	    throw new AuthenticationFailException();
	}
    }
}
